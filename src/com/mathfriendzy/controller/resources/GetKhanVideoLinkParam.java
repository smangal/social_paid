package com.mathfriendzy.controller.resources;

/**
 * Created by root on 5/8/16.
 */
public class GetKhanVideoLinkParam {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
