package com.mathfriendzy.controller.resources;

import java.io.Serializable;

/**
 * Created by root on 12/1/16.
 */
public class ResourceSubCat implements Serializable{

    private int subCatId;
    private String subCatName;

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }
}
