package com.mathfriendzy.controller.resources;


/**
 * Created by root on 19/8/15.
 */
public interface ResourceAdapterCallback {
    void onAddToHomework(ResourceResponse resources);
    void onResourceLock(int index);
}
