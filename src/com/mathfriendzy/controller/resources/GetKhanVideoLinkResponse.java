package com.mathfriendzy.controller.resources;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 5/8/16.
 */
public class GetKhanVideoLinkResponse extends HttpResponseBase{
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
