package com.mathfriendzy.controller.base;

import android.view.View.OnClickListener;
import android.widget.TextView;

import com.mathfriendzy.serveroperation.HttpResponseInterface;

abstract public class ActBase extends AdBaseActivity 
implements HttpResponseInterface , OnClickListener{
	protected TextView txtTopbar = null;
	/**
	 * This method get the widgets references from the xml and set it to the Objects
	 */
	protected abstract void setWidgetsReferences();

	/**
	 * This method set the listener on widgets
	 */
	protected abstract void setListenerOnWidgets();

	/**
	 * This method set the text from the transaltino
	 */
	protected abstract void setTextFromTranslation();
}
