package com.mathfriendzy.controller.result;

import static com.mathfriendzy.utils.ITextIds.MF_RESULT;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.result.JsonAsynTaskForDate;
import com.mathfriendzy.model.result.JsonAsyncTaskForScore;
import com.mathfriendzy.model.top100.JsonFileParser;
import com.mathfriendzy.utils.DialogGenerator;
import com.socialfriendzypaid.R;

public class SelectDateActivity extends AdBaseActivity implements OnItemClickListener, OnClickListener 
{
	private TextView txtTitle				= null;
	private ListView listDate				= null;
	private Button	 btnMoreDate			= null;
	private ArrayList<String> dateRecord	= null;
	
	private String playerName;
	private int userId;
	private int playerId;
	private int offset;
	private boolean flag;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_date);

		getWidgetId();
		setWidgetText();

		String jsonFile = getIntent().getStringExtra("jsonFile");
		playerName  = getIntent().getStringExtra("playerName");
		userId		= getIntent().getIntExtra("userId", 0);
		playerId	= getIntent().getIntExtra("playerId", 0);	
		offset		= getIntent().getIntExtra("offset", 0);
		
		JsonFileParser file = new JsonFileParser(this);
		dateRecord 			= new ArrayList<String>();

		dateRecord 			= file.getDateList(jsonFile);
		
		checkDateList();
		
		DateListAdapter adapter = new DateListAdapter(this, dateRecord);
		listDate.setAdapter(adapter);
				
	}

	private void checkDateList() 
	{
		flag = true;
		if(dateRecord.size() <= 0)
		{
			flag = false;
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateDateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgNoMoreResult"));
			transeletion.closeConnection();
		}
		else if(dateRecord.size() >= 30)
		{
			offset = offset + 30;
			btnMoreDate.setVisibility(View.VISIBLE);
		}
		else
		{
			btnMoreDate.setVisibility(View.GONE);
		}
		
	}

	private void getWidgetId() 
	{	
		txtTitle 	= (TextView) findViewById(R.id.txtTitleScreen);
		listDate 	= (ListView) findViewById(R.id.listDate);
		btnMoreDate	= (Button) findViewById(R.id.btnMoreDate);
		
		btnMoreDate.setOnClickListener(this);
		listDate.setOnItemClickListener(this);
	}//END getWidgetId method



	private void setWidgetText() 
	{
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier(MF_RESULT);
		txtTitle.setText(text);		

		text = translate.getTranselationTextByTextIdentifier("btnTitleShowMore");
		btnMoreDate.setText(text);	
		
		translate.closeConnection();		
	}//END setWidgetText method



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) 
	{
		new JsonAsyncTaskForScore(this, dateRecord.get(position), userId, playerId, playerName, flag)
			.execute(null,null,null);

	}

	@Override
	public void onClick(View v) 
	{
		new JsonAsynTaskForDate(this, userId, playerId,playerName,offset).execute(null,null,null);
	}
	
	
}
