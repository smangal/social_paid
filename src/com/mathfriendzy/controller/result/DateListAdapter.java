package com.mathfriendzy.controller.result;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mathfriendzy.utils.DateTimeOperation;
import com.socialfriendzypaid.R;

public class DateListAdapter extends BaseAdapter
{
	Context context;
	ArrayList<String> listDate;
	DateTimeOperation date;
	
	public DateListAdapter(Context context, ArrayList<String> listDate)
	{
		this.context = context;			
		this.listDate = new ArrayList<String>();
		this.listDate = listDate;
		date		= new DateTimeOperation();
	}

	@Override
	public int getCount() 
	{		
		return listDate.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View row, ViewGroup parent)
	{
		ViewHolder holder;
		if (row == null)
		{
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.date_list, parent, false);
			
			holder = new ViewHolder();
			holder.txtDate = (TextView) row.findViewById(R.id.txtDate);
			row.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) row.getTag();
		}
		
		holder.txtDate.setText(""+date.setDate(listDate.get(position)));
		
		return row;
	}
	
	
	class ViewHolder
	{
		TextView txtDate;
	}
	
}
