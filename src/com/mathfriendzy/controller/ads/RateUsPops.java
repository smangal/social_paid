package com.mathfriendzy.controller.ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mathfriendzy.controller.base.MyApplication;
import com.mathfriendzy.controller.moreapp.ActMoreApp;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ITextIds;
import com.socialfriendzypaid.R;


/**
 * Created by root on 13/10/15.
 */
public class RateUsPops {
	private static RateUsPops rateUsPopUP = null;
	private static Context myContext = null;
	private View view = null;
	private WindowManager wm = null;
	private static int NUMBER_OF_SCREEN_COUNTER = 0;
	private static final int MAX_SCREEN_COUNTER = 33;
	private WindowManager windowManage = null;

	private String lblInvalidEmail = "";
	private String lblPleaseEnterInfoBeforeSendingFeedback = "";
	private String appName = "";

	public static RateUsPops getInstance(Context context){
		myContext = context;
		if(rateUsPopUP == null)
			rateUsPopUP = new RateUsPops();
		return rateUsPopUP;
	}

	@SuppressWarnings("unused")
	public void showRateUsPopUp(){		
		/*if(MathFriendzyHelper.getBooleanValueFromPreff(myContext, 
				MathFriendzyHelper.SHOW_RATE_US_POP_UP_KEY)){
			return;
		}*/
		
		/*if(AppVersion.CURRENT_VERSION == AppVersion.PAID_VERSION){
			return ;
		}*/
		NUMBER_OF_SCREEN_COUNTER ++ ;
		//if(NUMBER_OF_SCREEN_COUNTER == MAX_SCREEN_COUNTER){		
		if(NUMBER_OF_SCREEN_COUNTER == this.getFrequency(myContext)){
			if(MathFriendzyHelper.getBooleanValueFromPreff(myContext, 
					MathFriendzyHelper.SHOW_RATE_US_POP_UP_KEY)){
				NUMBER_OF_SCREEN_COUNTER = 0;
				//HouseAds.getInstance().showCustomeAds(myContext);
			}else{
				this.showViewInWM(myContext);
			}
		}
	}

	@SuppressLint("InflateParams")
	private View getView(final Context context){
		LayoutInflater inflater = (LayoutInflater)context.getSystemService
				(Context.LAYOUT_INFLATER_SERVICE);
		View dialogView = inflater.inflate(R.layout.rate_us_enjoying_popup, null);

		TextView txtMsg  = (TextView) dialogView.findViewById(R.id.txtAreYouEnjoying);
		Button btnCancel = (Button) dialogView.findViewById(R.id.btnNoThanks);
		Button btnOk     = (Button) dialogView.findViewById(R.id.btnYes);

		Translation transeletion = new Translation(context);
		transeletion.openConnection(); 
		txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblAreYouEnjoying")
				+ "\n" + MathFriendzyHelper.getAppName(transeletion) + "?");
		btnCancel.setText(transeletion.getTranselationTextByTextIdentifier("lblNo"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleYes"));
		transeletion.closeConnection();

		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				removeViewFromWM(wm , view);
				showRateUsNoDialog(context);
			}
		});

		btnOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				removeViewFromWM(wm , view);
				if(MathFriendzyHelper.getBooleanValueFromPreff(myContext, 
						MathFriendzyHelper.SHOW_RATE_US_POP_UP_KEY)){
					openMoreGreatAppScreen(context);
				}else{
					showRateUsYesDialog(context);
				}
			}
		});
		return dialogView;
	}

	private void showViewInWM(Context context){
		try{
			WindowManager.LayoutParams params = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					0,
					WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
					| WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
					PixelFormat.TRANSLUCENT);
			params.gravity = Gravity.CENTER;
			wm = (WindowManager) MyApplication.getAppContext()
					.getSystemService(Context.WINDOW_SERVICE);
			view = this.getView(context);
			wm.addView(view, params);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void removeViewFromWM(WindowManager wm , View view){
		if(wm != null && view != null){
			NUMBER_OF_SCREEN_COUNTER = 0;
			wm.removeView(view);
		}
	}   

	private void addViewInWM(Context context , View view){
		try{
			WindowManager.LayoutParams params = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					0,
					WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
					| WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
					PixelFormat.TRANSLUCENT);
			params.gravity = Gravity.CENTER;
			if(windowManage == null)
				windowManage = (WindowManager) MyApplication.getAppContext()
				.getSystemService(Context.WINDOW_SERVICE);			
			windowManage.addView(view, params);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void removeViewFromWindoManage(WindowManager wm , View view){
		if(wm != null && view != null){			
			wm.removeView(view);
		}
	}

	@SuppressLint("InflateParams")
	private void showRateUsYesDialog(final Context context){
		LayoutInflater inflater = (LayoutInflater)context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View dialogView = inflater.inflate(R.layout.rate_us_enjoying_popup_yes, null);
		TextView txtThanks = (TextView) dialogView.findViewById(R.id.txtThanks);
		TextView txtMsg  = (TextView) dialogView.findViewById(R.id.txtAreYouEnjoying);
		Button btnRateUs = (Button) dialogView.findViewById(R.id.btnRateUs);
		Button btnRemindMeLater     = (Button) dialogView.findViewById(R.id.btnRemindMeLater);
		Button btnNoThanks = (Button) dialogView.findViewById(R.id.btnNoThanks);

		Translation transeletion = new Translation(context);
		transeletion.openConnection();  
		txtThanks.setText(transeletion.getTranselationTextByTextIdentifier("lblThankYou"));
		txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblWeAreHappy")
				+ " " + MathFriendzyHelper.getAppName(transeletion)
				+ ". " + transeletion.getTranselationTextByTextIdentifier("lblItWouldReally"));
		btnRateUs.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleRateUs"));
		btnRemindMeLater.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleRemindMeLater"));		
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		transeletion.closeConnection();

		btnRateUs.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {				
				removeViewFromWindoManage(windowManage, dialogView);
				MathFriendzyHelper.saveBooleanValueInSharedPreff(context, 
						MathFriendzyHelper.SHOW_RATE_US_POP_UP_KEY, true);									
				openMoreGreatAppScreen(context);
				MathFriendzyHelper.openUrl(context, ITextIds.RATE_URL);
			}
		});

		btnRemindMeLater.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				removeViewFromWindoManage(windowManage, dialogView);
				openMoreGreatAppScreen(context);
			}
		});

		btnNoThanks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {				
				removeViewFromWindoManage(windowManage, dialogView);
				MathFriendzyHelper.saveBooleanValueInSharedPreff(context, 
						MathFriendzyHelper.SHOW_RATE_US_POP_UP_KEY, true);
				openMoreGreatAppScreen(context);				
			}
		});

		addViewInWM(context , dialogView);
	}

	@SuppressLint("InflateParams")
	private void showRateUsNoDialog(final Context context){
		LayoutInflater inflater = (LayoutInflater)context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View dialogView = inflater.inflate(R.layout.rate_us_enjoying_pop_up_no, null);
		TextView txtThanks = (TextView) dialogView.findViewById(R.id.txtThanks);
		TextView txtPleaseLetUsKnow  = (TextView) dialogView.findViewById(R.id.txtPleaseLetUsKnow);
		final EditText edtEmail = (EditText) dialogView.findViewById(R.id.edtEmail);
		final EditText edtTellUs = (EditText) dialogView.findViewById(R.id.edtTellUs);
		Button btnNoThanks = (Button) dialogView.findViewById(R.id.btnNoThanks);
		Button btnSend = (Button) dialogView.findViewById(R.id.btnSend);

		Translation transeletion = new Translation(context);
		transeletion.openConnection();  
		txtThanks.setText(transeletion.getTranselationTextByTextIdentifier("lblThanksForFeedback"));
		txtPleaseLetUsKnow.setText(transeletion.getTranselationTextByTextIdentifier("lblPleaseLetUsKnow") 
				+ " " + MathFriendzyHelper.getAppName(transeletion)
				+ ".");		
		edtEmail.setHint(transeletion.getTranselationTextByTextIdentifier("lblYourEmailAddress"));
		edtTellUs.setHint(transeletion.getTranselationTextByTextIdentifier
				("lblTellUsHowWeCanHelp"));
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnSend.setText(transeletion.getTranselationTextByTextIdentifier("lblSend"));
		appName = MathFriendzyHelper.getAppName(transeletion);
		lblInvalidEmail = transeletion.
				getTranselationTextByTextIdentifier("alertMsgEmailIncorrectFormat");
		lblPleaseEnterInfoBeforeSendingFeedback = transeletion.
				getTranselationTextByTextIdentifier("lblPleaseEnterInfoBeforeSendingFeedback");
		transeletion.closeConnection();

		btnNoThanks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				removeViewFromWindoManage(windowManage, dialogView);
				openMoreGreatAppScreen(context);
			}
		});

		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//removeViewFromWindoManage(windowManage, dialogView);
				sendFeedback(context, edtEmail, 
						edtTellUs, new OnRequestComplete() {
					@Override
					public void onComplete() {						
						removeViewFromWindoManage(windowManage, dialogView);
						openMoreGreatAppScreen(context);
					}
				});
			}
		});
		addViewInWM(context , dialogView);
	}

	private void sendFeedback(Context context , 
			EditText edtEmail , EditText edtFeedback , 
			OnRequestComplete listner){
		String email = edtEmail.getText().toString();
		String feedback = edtFeedback.getText().toString();

		if(!MathFriendzyHelper.checkForEmptyFields(email , feedback)){
			if(CommonUtils.isEmailValid(email)){
				MathFriendzyHelper.sendFeedback(context, email, feedback , appName);
				listner.onComplete();
			}else{
				MathFriendzyHelper.showToast(context, lblInvalidEmail);
			}
		}else{
			MathFriendzyHelper.showToast(context, lblPleaseEnterInfoBeforeSendingFeedback);
		}
	}

	private int getFrequency(Context context){
		int frequency = MathFriendzyHelper.getIntValueFromPreff
				(context, MathFriendzyHelper.RATE_ADS_FREQUENCY_KEY); 		
		return frequency > 0 ? frequency : MAX_SCREEN_COUNTER;
	}

	private void openMoreGreatAppScreen(Context context){
		Intent intent = new Intent(context , ActMoreApp.class);
		intent.putExtra("isOpenAfterRateUsPopUp", true);
		context.startActivity(intent);
	}
}
