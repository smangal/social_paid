package com.mathfriendzy.controller.main;

import static com.mathfriendzy.utils.ICommonUtils.IMG_AVTAR_URL;
import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.MAIN_ACTIVITY_LOG;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.ads.GetAdsFrequencies;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.friendzy.StudentChallengeActivity;
import com.mathfriendzy.controller.help.ActHelpSchoolFriendzy;
import com.mathfriendzy.controller.learningcenter.LearningCenterMain;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.player.CreateTeacherPlayerActivity;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.PlayersActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.resources.ActLessonCategories;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.controller.videoview.ActPlayVideo;
import com.mathfriendzy.dawnloadimagesfromserver.DawnLoadAvtarFromServerThread;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.AvtarDTO;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerDataFromServerObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyServerOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.AppVersion;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.socialfriendzypaid.R;


/**
 * This is the main activity where the application home is display to play it
 * @author Yashwant Singh
 *
 */
public class MainActivity extends AdBaseActivity implements OnClickListener
{
	private RelativeLayout learningCenterLayout = null;
	private RelativeLayout singleFriendzyLayout = null;
	private RelativeLayout multiFriendzyLayout  = null;
	private TextView mfTitleHomeScreen 			= null;
	private TextView lblLearningCenter 			= null;
	private TextView lblVideosFlashCardsPlay 	= null;
	private TextView lblSingleFriendzy 			= null;
	private TextView lblPlayAroundTheWorld 		= null;
	private TextView lblBestOf5Friendzy 		= null;
	private TextView lblPlayWithYourFriends 	= null;
	private Button   btnTitlePlayers 			= null;
	private TextView lblTheGameOfMath			= null;
	private Button   btnTitleTop100         	= null;

	private ArrayList<AvtarDTO> avtarDataList   = null;
	public static boolean isAvtarLoaded 		= false;
	private final String TAG					= this.getClass().getSimpleName();

	public static int IS_CALL_FROM_LEARNING_CENTER = 0;
	public static int IS_CALL_FROM_SINGLE_FRIENDZY = 0;
	public static int IS_CALL_FROM_MULTIFRIENDZY   = 0;

	private final int MAX_ITEM_ID = 11;// if item id is 11 the unlock the level
	private final int MAX_ITEM_ID_FOR_ALL_APP = 100;// if item id is 12 the unlock the level

	@SuppressWarnings("unused")
	private int isClickOn = 0;
	@SuppressWarnings("unused")
	private boolean isCompleteLevelLoadFromSerevr = false;//single friendzy
	private  int SCREEN_DENISITY   = 240;
	//private SharedPreferences sharedPreference 	= null;
	public static boolean isTab						= false;

	private RelativeLayout freeSubscriptiontextLayout = null;
	private TextView txtFreeSubscriptionText = null;
	private final String CURRENT_EXPIRE_DATE_FORMAT = "yyyy-MM-dd";
	private final String REQUIRED_EXPIRE_DATE_FORMAT = "MMMM dd, yyyy";
	private final String CURRENT_SUBSCRIPTION_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private String lblYourSchoolSubscriptionExpire = "";
	private String lblFreeStudentTeacherFunctionExpire = "";
	private String lblRegisterLoginToUseSchoolFree = "";

	private String alertMsgYouMustRegisterOrLogin = null;
	
	//new change jan 2016
	private RelativeLayout rlVedioLessionLayout = null;
	private RelativeLayout rlRequestATutor = null;
	private TextView txtVedioLession = null;
	private TextView txtRequestATutor = null;
		
	private RelativeLayout rlSchoolFriendzyCheckout = null;
	private TextView txtCheckOut = null;
	private ImageView imgSchoolLogo = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		isHomeScreen = true;
		isTab = getResources().getBoolean(R.bool.isTabSmall);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		boolean tabletSize = getResources().getBoolean(R.bool.isTabSmall);

		if(tabletSize)
		{
			if (metrics.densityDpi > 160){
				setContentView(R.layout.activity_main);
			}
			else
			{
				setContentView(R.layout.tab_ten_inch_activity_main);
			}
		}
		else
		{
			if(metrics.heightPixels == CommonUtils.TAB_HEIGHT && metrics.widthPixels == CommonUtils.TAB_WIDTH
					&& metrics.densityDpi == CommonUtils.TAB_DENISITY)  {

				setContentView(R.layout.activity_main_tab_loe_denisity);
			}
			else if ((getResources().getConfiguration().screenLayout &
					Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					metrics.densityDpi > SCREEN_DENISITY)
			{
				setContentView(R.layout.activity_main_activity_large);
			}
		}		

		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " inside onCreate()");

		this.getWidgetsReferences();
		this.setWidgetsText();
		this.setListenerOnWidgets();

		if(! isAvtarLoaded)//check for avtar is loaded or not, if loaded then it will not load again
		{
			//Log.e(TAG, "inside avtar load " + isAvtarLoaded);
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				new GetAvtarFromServer().execute(null,null,null);
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
		}

		//Log.e(TAG, "outside avtar load " + isAvtarLoaded);
		/*
		 * Changes Of 14th Jan
		 */
		if(this.getIntent().getBooleanExtra("isCallFromRegistration", false)){
			CommonUtils.showDialogAfterRegistrationSuccess(this);
		}
		
		//New change jan 2016
		this.getInAppPurchaseStatus();
		
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " outside onCreate()");
	}


	@Override
	protected void onResume() {
		isHomeScreen = true;		
		if(CommonUtils.isReadyToGetNewFrequesncyFromSerevr(this)
				&& CommonUtils.isInternetConnectionAvailable(this)){
			new GetAdsFrequencies(this).execute(null,null,null);
		}
		this.setSubscriptionRemainingDays();
		super.onResume();
	}

	/**
	 * This method get references from layout and set it to the widgets
	 */
	private void getWidgetsReferences()
	{
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " inside getWidgetsReferences()");

		learningCenterLayout    = (RelativeLayout) findViewById(R.id.learningCenterLayout);
		singleFriendzyLayout    = (RelativeLayout) findViewById(R.id.singleFriendzyLayout);
		multiFriendzyLayout     = (RelativeLayout) findViewById(R.id.multiFriendzyLayout);
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblLearningCenter 		= (TextView) findViewById(R.id.lblLearningCenter);
		lblVideosFlashCardsPlay = (TextView) findViewById(R.id.lblVideosFlashCardsPlay);
		lblSingleFriendzy 		= (TextView) findViewById(R.id.lblSingleFriendzy);
		lblPlayAroundTheWorld 	= (TextView) findViewById(R.id.lblPlayAroundTheWorld);
		lblBestOf5Friendzy 		= (TextView) findViewById(R.id.lblBestOf5Friendzy);
		lblPlayWithYourFriends 	= (TextView) findViewById(R.id.lblPlayWithYourFriends);
		btnTitlePlayers 		= (Button) findViewById(R.id.btnTitlePlayers);
		lblTheGameOfMath		= (TextView) findViewById(R.id.lblTheGameOfMath);
		btnTitleTop100			= (Button) findViewById(R.id.btnTitleTop100);	


		freeSubscriptiontextLayout = (RelativeLayout) findViewById(R.id.freeSubscriptiontextLayout);
		txtFreeSubscriptionText    = (TextView) findViewById(R.id.txtFreeSubscriptionText);
		
		
		//New change Jan 2016
		rlVedioLessionLayout = (RelativeLayout) findViewById(R.id.rlVedioLessionLayout);
		rlRequestATutor = (RelativeLayout) findViewById(R.id.rlRequestATutor);
		txtVedioLession = (TextView) findViewById(R.id.txtVedioLession);
		txtRequestATutor = (TextView) findViewById(R.id.txtRequestATutor);
		
		rlSchoolFriendzyCheckout = (RelativeLayout) findViewById(R.id.rlSchoolFriendzyCheckout);
		txtCheckOut = (TextView) findViewById(R.id.txtCheckOut);
		imgSchoolLogo = (ImageView) findViewById(R.id.imgSchoolLogo);
		
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " outside getWidgetsReferences()");
	}

	/**
	 * This method set the widgets text values from the translation
	 */
	private void setWidgetsText()
	{
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " inside setWidgetsText()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(transeletion));
		//		mfTitleHomeScreen.setText(ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE));
		lblLearningCenter.setText(transeletion.getTranselationTextByTextIdentifier("lblLearningCenter"));
		lblVideosFlashCardsPlay.setText(transeletion.getTranselationTextByTextIdentifier("lblVideosFlashCardsPlay"));
		lblSingleFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblSingleFriendzy"));
		lblPlayAroundTheWorld.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayAroundTheWorld"));
		lblBestOf5Friendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
		lblPlayWithYourFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayWithYourFriends"));
		btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("lblFriendzyChallenge"));
		lblTheGameOfMath.setText(transeletion.getTranselationTextByTextIdentifier("lblTheGameOfLearning") + "!");
		btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("lblRateMe"));
		
		lblYourSchoolSubscriptionExpire = transeletion
				.getTranselationTextByTextIdentifier("lblYourSchoolSubscriptionExpire");
		lblFreeStudentTeacherFunctionExpire = transeletion
				.getTranselationTextByTextIdentifier("lblFreeStudentTeacherFunctionExpire");
		lblRegisterLoginToUseSchoolFree = transeletion
				.getTranselationTextByTextIdentifier("lblRegisterLoginToUse")
				+ " " + transeletion
				.getTranselationTextByTextIdentifier("btnTitleFree")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblFor30days");
		alertMsgYouMustRegisterOrLogin = transeletion
				.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin");
		//New change jan 2016
		txtVedioLession.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormatVideos")
				+ " & " + transeletion.getTranselationTextByTextIdentifier("lblLesson"));
		txtRequestATutor.setText(transeletion
				.getTranselationTextByTextIdentifier("lblRequestATutor"));
		txtCheckOut.setText(transeletion
				.getTranselationTextByTextIdentifier("lblCheckOut") + ": ");
		transeletion.closeConnection();
		
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " outside setWidgetsText()");
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets()
	{
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " inside setListenerOnWidgets()");

		learningCenterLayout.setOnClickListener(this);
		singleFriendzyLayout.setOnClickListener(this);
		multiFriendzyLayout.setOnClickListener(this);
		btnTitlePlayers.setOnClickListener(this);
		btnTitleTop100.setOnClickListener(this);

		//New change Jan 2016
		rlVedioLessionLayout.setOnClickListener(this);
		rlRequestATutor.setOnClickListener(this);
			
		rlSchoolFriendzyCheckout.setOnClickListener(this);
		
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " outside setListenerOnWidgets()");
	}

	@Override
	public void onClick(View v)
	{	
		switch(v.getId())
		{
		case R.id.learningCenterLayout :
			//showHouseAd();
			/*this.checkForTempPlayer();*/
			TempPlayerOperation tempObj = new TempPlayerOperation(this);
			if(tempObj.isTemparyPlayerExist())
			{
				tempObj.closeConn();
				this.clickOnLearningCenter();
			}
			else
			{
				tempObj.createTempPlayer("LearningCenterMain");
				//this.clickOnLearningCenter();
			}

			break;
		case R.id.singleFriendzyLayout : 
			//showHouseAd();
			//startActivity(new Intent(this,SingleFriendzyMain.class));
			TempPlayerOperation tempObj1 = new TempPlayerOperation(this);
			if(tempObj1.isTemparyPlayerExist())
			{
				tempObj1.closeConn();
				this.clickOnSingleFriendzy();

			}
			else
			{
				tempObj1.createTempPlayer("SingleFriendzyMain");
				//this.clickOnLearningCenter();
			}

			break;
		case R.id.multiFriendzyLayout : 
			//showHouseAd();
			if(!CommonUtils.isInternetConnectionAvailable(this))
			{
				CommonUtils.showInternetDialog(this);			
			}
			else
			{
				TempPlayerOperation tempObj2 = new TempPlayerOperation(this);
				if(tempObj2.isTemparyPlayerExist())
				{
					tempObj2.closeConn();
					this.clickOnMultiFriendzy();
				}
				else
				{
					tempObj2.createTempPlayer("MultiFriendzyMain");
					//this.clickOnLearningCenter();
				}
			}

			break;
		case R.id.btnTitlePlayers :
			//showHouseAd();
			checkPlayer();
			//this.checkForTempPlayer();
			break;
		case R.id.btnTitleTop100 :
			//			showHouseAd();
			MathFriendzyHelper.rateUs(this);
			/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}*/
			break;
		case R.id.rlVedioLessionLayout:
			this.clickOnVedioAndLessons();
			break;
		case R.id.rlRequestATutor:
			this.clickOnRequestATutor();
			break;
		case R.id.rlSchoolFriendzyCheckout:
			this.clickOnCheckout();
			break;
		}
	}

	/**
	 * This method call when user click on multifriendzy
	 */
	private void clickOnMultiFriendzy()
	{
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " inside clickOnMultiFriendzy()");

		//check for user login or not in not then check for temp player existence

		//shared preff for holding chacked player for playing
		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);

		//sharedpreference for holding player info
		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
			if(!tempPlayer.isTempPlayerDeleted())
			{
				TempPlayerOperation tempPlayer1 = new TempPlayerOperation(this);
				ArrayList<TempPlayer> tempPlayerData = tempPlayer1.getTempPlayerData();

				Intent intent = new Intent(this,MultiFriendzyMain.class);

				editor.clear();
				editor.putString("playerName", tempPlayerData.get(0).getFirstName() + " " + tempPlayerData.get(0).getLastName());
				editor.putString("city", tempPlayerData.get(0).getCity());
				editor.putString("state", tempPlayerData.get(0).getState());
				editor.putString("imageName",  tempPlayerData.get(0).getProfileImageName());
				editor.putString("coins",  String.valueOf(tempPlayerData.get(0).getCoins()));
				editor.putInt("grade", tempPlayerData.get(0).getGrade());

				//for score update
				editor.putString("userId",  "0");
				editor.putString("playerId",  "0");

				editor.putString("countryName",tempPlayerData.get(0).getCountry());
				editor.commit();	

				startActivity(intent);
			}
			else
			{
				/*Intent intent = new Intent(this,CreateTempPlayerActivity.class);
				startActivity(intent);*/
				MathFriendzyHelper.openCreateTempPlayerActivity(this);
			}
		}
		else
		{
			UserPlayerOperation userPlayer = new UserPlayerOperation(this);
			if(!userPlayer.isUserPlayersExist())
			{
				SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
				if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
				{			
					IS_CALL_FROM_MULTIFRIENDZY = 1;
					Intent intent = new Intent(this,LoginUserPlayerActivity.class);
					startActivity(intent);
				}
				else
				{
					UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
					UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

					if(userPlayerData != null )
					{
						//changes
						SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
						singleimpl.openConn();
						int itemId = singleimpl.getItemIdForMultiFriendzy(userPlayerData.getParentUserId());
						singleimpl.closeConn();
						
						if(itemId != MAX_ITEM_ID_FOR_ALL_APP)
						{
							if(CommonUtils.isInternetConnectionAvailable(this))
							{
								//Log.e("", "not max 100");
								new GetPurchsedItemDetailByUserId(userPlayerData.getParentUserId(), 
										this).execute(null,null,null);
							}
						}
						//changes end
						else
						{
							this.setUserDetailForMultiFriendzy();
						}
					}
					else
					{
						IS_CALL_FROM_MULTIFRIENDZY = 1;
						Intent intent = new Intent(this,LoginUserPlayerActivity.class);
						startActivity(intent);
					}
				}
			}
			else
			{
				Intent intent = new Intent(this,LoginUserCreatePlayer.class);
				startActivity(intent);
			}
		}

		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " outside clickOnMultiFriendzy()");
	}


	/**
	 * This method set the user detail for multifriendzy
	 */
	private void setUserDetailForMultiFriendzy()
	{		
		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

		SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
		UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
		UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		RegistereUserDto regUserObj = userObj.getUserData();

		Intent intent = new Intent(this,MultiFriendzyMain.class);

		editor.clear();
		editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
		editor.putString("userName", userPlayerData.getUsername());
		editor.putString("city", regUserObj.getCity());
		editor.putString("state", regUserObj.getState());
		editor.putString("imageName",  userPlayerData.getImageName());
		editor.putString("coins",  userPlayerData.getCoin());
		editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
		editor.putString("userId",  userPlayerData.getParentUserId());
		editor.putString("playerId",  userPlayerData.getPlayerid());
		Country country = new Country();
		editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
		editor.commit();

		startActivity(intent);	
	}

	/**
	 * This method call when the click perform on learninf center 
	 * it will check for already login player or not
	 * if already login then get the detail from the user player data other wise get detail form temp player 
	 */
	private void clickOnLearningCenter() 
	{
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " inside clickOnLearningCenter()");

		//check for user login or not in not then check for temp player existence

		//shared preff for holding chacked player for playing
		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);

		//sharedpreference for holding player info
		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
			if(!tempPlayer.isTempPlayerDeleted())
			{
				TempPlayerOperation tempPlayer1 = new TempPlayerOperation(this);
				ArrayList<TempPlayer> tempPlayerData = tempPlayer1.getTempPlayerData();

				Intent intent = new Intent(this,LearningCenterMain.class);

				editor.clear();
				editor.putString("playerName", tempPlayerData.get(0).getFirstName() + " " + tempPlayerData.get(0).getLastName());
				editor.putString("city", tempPlayerData.get(0).getCity());
				editor.putString("state", tempPlayerData.get(0).getState());
				editor.putString("imageName",  tempPlayerData.get(0).getProfileImageName());
				editor.putString("coins",  String.valueOf(tempPlayerData.get(0).getCoins()));
				editor.putInt("grade", tempPlayerData.get(0).getGrade());

				//for score update
				editor.putString("userId",  "0");
				editor.putString("playerId",  "0");

				editor.putString("countryName",tempPlayerData.get(0).getCountry());
				editor.commit();	

				startActivity(intent);
			}
			else
			{
				/*Intent intent = new Intent(this,CreateTempPlayerActivity.class);
				startActivity(intent);*/
				MathFriendzyHelper.openCreateTempPlayerActivity(this);
			}
		}
		else
		{
			UserPlayerOperation userPlayer = new UserPlayerOperation(this);
			if(!userPlayer.isUserPlayersExist())
			{
				SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
				if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
				{			
					IS_CALL_FROM_LEARNING_CENTER = 1;
					Intent intent = new Intent(this,LoginUserPlayerActivity.class);
					startActivity(intent);
				}
				else
				{
					UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
					UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

					if(userPlayerData != null )
					{
						UserRegistrationOperation userObj = new UserRegistrationOperation(this);
						RegistereUserDto regUserObj = userObj.getUserData();

						Intent intent = new Intent(this,LearningCenterMain.class);

						editor.clear();
						editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
						editor.putString("city", regUserObj.getCity());
						editor.putString("userName", userPlayerData.getUsername());
						editor.putString("state", regUserObj.getState());
						editor.putString("imageName",  userPlayerData.getImageName());
						editor.putString("coins",  userPlayerData.getCoin());
						editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
						editor.putString("userId",  userPlayerData.getParentUserId());
						editor.putString("playerId",  userPlayerData.getPlayerid());
						Country country = new Country();
						editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
						editor.commit();

						startActivity(intent);
					}
					else
					{
						IS_CALL_FROM_LEARNING_CENTER = 1;
						Intent intent = new Intent(this,LoginUserPlayerActivity.class);
						startActivity(intent);
					}
				}
			}
			else
			{
				Intent intent = new Intent(this,LoginUserCreatePlayer.class);
				startActivity(intent);
			}
		}

		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " outside clickOnLearningCenter()");
	}

	/**
	 * This method call when user click on single friendzy
	 */
	private void clickOnSingleFriendzy()
	{
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " inside clickOnSingleFriendzy()");

		//check for user login or not in not then check for temp player existence

		//shared preff for holding chacked player for playing
		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);

		//sharedpreference for holding player info
		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
			if(!tempPlayer.isTempPlayerDeleted())
			{
				TempPlayerOperation tempPlayer1 = new TempPlayerOperation(this);
				ArrayList<TempPlayer> tempPlayerData = tempPlayer1.getTempPlayerData();

				Intent intent = new Intent(this,SingleFriendzyMain.class);

				editor.clear();
				editor.putString("playerName", tempPlayerData.get(0).getFirstName() + " " + tempPlayerData.get(0).getLastName());
				editor.putString("city", tempPlayerData.get(0).getCity());
				editor.putString("state", tempPlayerData.get(0).getState());
				editor.putString("imageName",  tempPlayerData.get(0).getProfileImageName());
				editor.putString("coins",  String.valueOf(tempPlayerData.get(0).getCoins()));
				editor.putInt("grade", tempPlayerData.get(0).getGrade());

				LearningCenterimpl learningCenter = new LearningCenterimpl(MainActivity.this);
				learningCenter.openConn();
				if(learningCenter.getDataFromPlayerTotalPoints("0")
						.getCompleteLevel() != 0)
					editor.putInt("completeLevel", MathFriendzyHelper
							.getNonZeroValue(learningCenter.getDataFromPlayerTotalPoints("0")
									.getCompleteLevel()));
				else
					editor.putInt("completeLevel",  MathFriendzyHelper
							.getNonZeroValue(tempPlayerData.get(0).getCompeteLevel()));
				learningCenter.closeConn();

				//editor.putInt("completeLevel", tempPlayerData.get(0).getCompeteLevel());
				//for score update
				editor.putString("userId",  "0");
				editor.putString("playerId",  "0");

				editor.putString("countryName",tempPlayerData.get(0).getCountry());
				editor.commit();	

				startActivity(intent);
			}
			else
			{
				/*Intent intent = new Intent(this,CreateTempPlayerActivity.class);
				startActivity(intent);*/
				MathFriendzyHelper.openCreateTempPlayerActivity(this);
			}
		}
		else
		{
			UserPlayerOperation userPlayer = new UserPlayerOperation(this);
			if(!userPlayer.isUserPlayersExist())
			{
				SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
				if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
				{			
					IS_CALL_FROM_SINGLE_FRIENDZY = 1;
					Intent intent = new Intent(this,LoginUserPlayerActivity.class);
					startActivity(intent);
				}
				else
				{
					UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
					UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

					if(userPlayerData != null )
					{
						SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
						singleimpl.openConn();
						int itemId = singleimpl.getItemId(userPlayerData.getParentUserId());
						singleimpl.closeConn();

						if(itemId != MAX_ITEM_ID)
						{
							if(CommonUtils.isInternetConnectionAvailable(this))
							{
								new GetSingleFriendzyDetail(userPlayerData.getParentUserId(), 
										userPlayerData.getPlayerid() , this).execute(null,null,null);
							}
							else
							{
								/*DialogGenerator dg = new DialogGenerator(this);
								Translation transeletion = new Translation(this);
								transeletion.openConnection();
								dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
								transeletion.closeConnection();*/
								setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(this);
							}

						}
						else
						{							
							if(CommonUtils.isInternetConnectionAvailable(this))
							{
								new GetpointsLevelDetail(userPlayerData.getParentUserId(), 
										userPlayerData.getPlayerid() , this).execute(null,null,null);
							}
							else
							{
								/*DialogGenerator dg = new DialogGenerator(this);
								Translation transeletion = new Translation(this);
								transeletion.openConnection();
								dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
								transeletion.closeConnection();*/
								setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(this);
							}
						}
					}
					else
					{
						IS_CALL_FROM_SINGLE_FRIENDZY = 1;
						Intent intent = new Intent(this,LoginUserPlayerActivity.class);
						startActivity(intent);
					}
				}
			}
			else
			{
				Intent intent = new Intent(this,LoginUserCreatePlayer.class);
				startActivity(intent);
			}
		}

		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " outside clickOnSingleFriendzy()");
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{	
			/*moveTaskToBack(true);
			finish();
			//System.exit(0);
			 */			
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * This method check for temp player is exist or not
	 */
	@SuppressWarnings("unused")
	private void checkForTempPlayer()
	{
		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " inside checkForTempPlayer()");

		TempPlayerOperation tempObj = new TempPlayerOperation(this);
		if(tempObj.isTemparyPlayerExist())
		{
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(sheredPreference.getBoolean(IS_LOGIN, false))
			{
				UserPlayerOperation userPlayer = new UserPlayerOperation(this);
				if(userPlayer.isUserPlayersExist())
				{
					UserRegistrationOperation userObj = new UserRegistrationOperation(this);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
						startActivity(intent);
					}
					else
					{
						Intent intent = new Intent(this,LoginUserCreatePlayer.class);
						startActivity(intent);
					}
				}
				else
				{
					UserRegistrationOperation userObj = new UserRegistrationOperation(this);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(this,TeacherPlayer.class);
						startActivity(intent);
					}
					else
					{
						Intent intent = new Intent(this,LoginUserPlayerActivity.class);
						startActivity(intent);
					}
				}
			}
			else
			{
				if(tempObj.isTempPlayerDeleted())//if temp player exist then check for temp player is deleted or not from the table
				{
					/*Intent intentCreate = new Intent(this,CreateTempPlayerActivity.class);
					startActivity(intentCreate);*/
					MathFriendzyHelper.openCreateTempPlayerActivity(this);
				}
				else
				{
					Intent intent  = new Intent(this,PlayersActivity.class);
					this.startActivity(intent);
				}
			}
			tempObj.closeConn();
		}
		else
		{
			tempObj.createTempPlayer("");//create temp player if not exists
		}

		//tempObj.closeConn();

		if(MAIN_ACTIVITY_LOG)
			Log.e(TAG, " outside checkForTempPlayer()");
	}



	/**
	 * This method call after the data from server is loaded for single friendzy
	 */
	private void setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(Context context)
	{
		SharedPreferences sheredPreferencePlayer = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
		UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
		UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

		SharedPreferences sharedPrefPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

		UserRegistrationOperation userObj = new UserRegistrationOperation(context);
		RegistereUserDto regUserObj = userObj.getUserData();

		Intent intent = new Intent(context,SingleFriendzyMain.class);

		editor.clear();
		editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
		editor.putString("city", regUserObj.getCity());
		editor.putString("state", regUserObj.getState());
		editor.putString("imageName",  userPlayerData.getImageName());
		editor.putString("userName", userPlayerData.getUsername());
		editor.putString("coins",  userPlayerData.getCoin());
		editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));

		editor.putString("userId",  userPlayerData.getParentUserId());
		editor.putString("playerId",  userPlayerData.getPlayerid());

		Country country = new Country();
		editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), context));

		LearningCenterimpl learningCenter = new LearningCenterimpl(context);
		learningCenter.openConn();

		if(learningCenter.getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
				.getCompleteLevel() != 0)
			editor.putInt("completeLevel", 
					MathFriendzyHelper
					.getNonZeroValue(learningCenter.getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
							.getCompleteLevel()));
		else
			try{
				editor.putInt("completeLevel",MathFriendzyHelper
						.getNonZeroValue( 
								Integer.parseInt(userPlayerData.getCompletelavel())));
			}
		catch(Exception e)
		{
			editor.putInt("completeLevel", 1);
		}

		learningCenter.closeConn();
		//for score update
		editor.commit();

		context.startActivity(intent);

	}


	/**
	 * This asynckTask get Avtar from server and save it
	 * @author Yashwant Singh
	 *
	 */
	class GetAvtarFromServer extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " inside GetAvtarFromServer doInBackground()");

			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			avtarDataList = chooseAvtarObj.getAvtar();

			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " outside GetAvtarFromServer doInBackground()");

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " inside GetAvtarFromServer onPostExecute()");

			//new GetImageBitmap(avtarDataList).execute(null,null,null);

			DawnLoadAvtarFromServerThread dawnlLoadAvter = 
					new DawnLoadAvtarFromServerThread(avtarDataList, MainActivity.this);
			Thread avtardDawnLoadThread = new Thread(dawnlLoadAvter);
			avtardDawnLoadThread.start();
			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " outside GetAvtarFromServer onPostExecute()");

			super.onPostExecute(result);
		}
	}

	/** 
	 * This asyncTask get Bitmap from url and save it one by one in to database
	 * @author Yashwant Singh
	 *
	 */
	class GetImageBitmap extends AsyncTask<Void, Integer, Void>
	{
		private String strUrl 				= null;
		ArrayList<AvtarDTO> avtarDataList 	= null;
		ArrayList<Bitmap>  bitmapList 		= null;
		ChooseAvtarOpration chooseAvtarObj  = null;

		public GetImageBitmap(ArrayList<AvtarDTO> avtarDataList) 
		{
			this.avtarDataList = avtarDataList;
		}

		@Override
		protected void onPreExecute() 
		{
			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " inside GetImageBitmap onPreExecute()");

			bitmapList = new ArrayList<Bitmap>();

			chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(MainActivity.this);
			if(chooseAvtarObj.isAvtarTableExist())
			{
				chooseAvtarObj.deleteFromAvtar();
			}
			else
			{
				chooseAvtarObj.createAvtarTable();
			}

			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " outside GetImageBitmap onPreExecute()");

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... arg0) 
		{
			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " inside GetImageBitmap doInBackground()");
			AvtarDTO avtarObj = null;
			try 
			{
				for( int i = 0 ; i < avtarDataList.size() ; i++)
				{
					if(CommonUtils.isInternetConnectionAvailable(MainActivity.this))
					{
						avtarObj = new AvtarDTO();
						strUrl = IMG_AVTAR_URL + avtarDataList.get(i).getName();
						//avtarDataList.get(i).setImage(CommonUtils.getByteFromBitmap(getBitmap(strUrl)));
						avtarObj.setId(avtarDataList.get(i).getId());
						avtarObj.setName(avtarDataList.get(i).getName());
						avtarObj.setPrice(avtarDataList.get(i).getPrice());

						//Log.e(TAG, "inside do in background image Url " + strUrl);
						avtarObj.setImage(CommonUtils.getByteFromBitmap(CommonUtils.getBitmap(strUrl + ".png")));
						chooseAvtarObj.insertAvtar(avtarObj);
					}
					else
					{
						break;
					}
				}
			}
			catch (MalformedURLException e) 
			{
				e.printStackTrace();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}	

			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " outside GetImageBitmap doInBackground()");

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{	
			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " inside GetImageBitmap onPostExecute()");

			chooseAvtarObj.closeConn();
			if(CommonUtils.isInternetConnectionAvailable(MainActivity.this))
			{
				isAvtarLoaded = true;
			}
			else
			{
				isAvtarLoaded = false;
			}

			if(MAIN_ACTIVITY_LOG)
				Log.e(TAG, " outside GetImageBitmap onPostExecute()");

			super.onPostExecute(result);
		}
	}	


	/**
	 * This asyncktask get single friendzy detail from server
	 * @author Yashwant Singh
	 *
	 */
	public class GetSingleFriendzyDetail extends AsyncTask<Void, Void, PlayerDataFromServerObj>
	{
		private String userId 	= null;
		private String playerId = null;
		private Context context = null;
		private ProgressDialog pd;

		public GetSingleFriendzyDetail(String userId, String playerId , Context context)
		{
			this.userId 	= userId;
			this.playerId 	= playerId;
			this.context    = context;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected PlayerDataFromServerObj doInBackground(Void... params) 
		{
			SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
			PlayerDataFromServerObj playerDetaill = singleServerObj.getSingleFriendzyDetail(userId, playerId);

			return playerDetaill;
		}

		@Override
		protected void onPostExecute(PlayerDataFromServerObj result) 
		{
			pd.cancel();

			String itemIds = "";

			ArrayList<String> itemList = new ArrayList<String>();

			if(result.getItems().length() > 0)
			{	
				//itemIds = result.getItems().replace(",", "");
				//get item value which is comma separated , add (,) at the last for finish	
				String newItemList = result.getItems() + ",";

				for(int i = 0 ; i < newItemList.length() ; i ++ )
				{
					if(newItemList.charAt(i) == ',' )
					{
						itemList.add(itemIds);
						itemIds = "";
					}
					else
					{
						itemIds = itemIds +  newItemList.charAt(i);
					}
				}
			}

			ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();

			for( int i = 0 ; i < itemList.size() ; i ++ )
			{
				PurchaseItemObj purchseObj = new PurchaseItemObj();
				purchseObj.setUserId(userId);
				purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
				purchseObj.setStatus(1);
				purchaseItem.add(purchseObj);
			}

			if(result.getLockStatus() != -1)
			{
				PurchaseItemObj purchseObj = new PurchaseItemObj();
				purchseObj.setUserId(userId);
				purchseObj.setItemId(100);
				purchseObj.setStatus(result.getLockStatus());
				purchaseItem.add(purchseObj);
			}

			LearningCenterimpl learningCenter = new LearningCenterimpl(context);
			learningCenter.openConn();

			if(learningCenter.isPlayerRecordExits(playerId))
			{
				learningCenter.updatePlayerPointsAndCmpleteLevel(result.getPoints(),
						result.getCompleteLevel(), playerId);
			}
			else
			{
				PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
				playerTotalPoints.setUserId(userId);
				playerTotalPoints.setPlayerId(playerId);
				playerTotalPoints.setCoins(0);
				playerTotalPoints.setCompleteLevel(result.getCompleteLevel());
				playerTotalPoints.setPurchaseCoins(0);

				int point = 0;
				try{
					point = result.getPoints();
				}
				catch(Exception e){}					
				playerTotalPoints.setTotalPoints(point);

				//playerTotalPoints.setTotalPoints(result.getPoints());
				learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
			}

			learningCenter.deleteFromPurchaseItem();
			learningCenter.insertIntoPurchaseItem(purchaseItem);		
			learningCenter.closeConn();

			setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(context);
			super.onPostExecute(result);

		}
	}

	/**
	 * this class get the player point detail from server for single friendzy
	 * @author Yashwant Singh
	 *
	 */
	public class GetpointsLevelDetail extends AsyncTask<Void, Void, PlayerDataFromServerObj>
	{
		private String userId 	= null;
		private String playerId = null;
		private Context context = null;
		private ProgressDialog pd;

		public GetpointsLevelDetail(String userId, String playerId , Context context)
		{
			this.userId 	= userId;
			this.playerId 	= playerId;
			this.context    = context;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected PlayerDataFromServerObj doInBackground(Void... params) 
		{
			SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
			PlayerDataFromServerObj playerDetaill = singleServerObj.getPointsLevelDetail(userId, playerId);
			return playerDetaill;
		}

		@Override
		protected void onPostExecute(PlayerDataFromServerObj result) 
		{
			pd.cancel();

			LearningCenterimpl learningCenter = new LearningCenterimpl(context);
			learningCenter.openConn();

			ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
			if(result.getLockStatus() != -1)
			{
				PurchaseItemObj purchseObj = new PurchaseItemObj();
				purchseObj.setUserId(userId);
				purchseObj.setItemId(100);
				purchseObj.setStatus(result.getLockStatus());
				purchaseItem.add(purchseObj);
				learningCenter.deleteFromPurchaseItem(100);
				learningCenter.insertIntoPurchaseItem(purchaseItem);	
			}

			if(learningCenter.isPlayerRecordExits(playerId))
			{
				learningCenter.updatePlayerPointsAndCmpleteLevel(result.getPoints(), result.getCompleteLevel(), playerId);
			}
			else
			{
				PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
				playerTotalPoints.setUserId(userId);
				playerTotalPoints.setPlayerId(playerId);
				playerTotalPoints.setCoins(0);
				playerTotalPoints.setCompleteLevel(result.getCompleteLevel());
				playerTotalPoints.setPurchaseCoins(0);

				int point = 0;
				try{
					point = result.getPoints();
				}
				catch(Exception e){}					
				playerTotalPoints.setTotalPoints(point);

				//playerTotalPoints.setTotalPoints(result.getPoints());
				learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
			}

			learningCenter.closeConn();

			setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(context);
			super.onPostExecute(result);
		}
	}


	/**
	 * This asyncktask get single friendzy detail from server
	 * @author Yashwant Singh
	 *
	 */
	public class GetPurchsedItemDetailByUserId extends AsyncTask<Void, Void, Void>
	{
		private String userId 	= null;
		private Context context = null;
		private String resultString = "";
		private ProgressDialog pd;

		public GetPurchsedItemDetailByUserId(String userId, Context context)
		{
			this.userId 	= userId;
			this.context    = context;
		}

		@Override
		protected void onPreExecute() 
		{
			setUserDetailForMultiFriendzy();
			//pd = CommonUtils.getProgressDialog(context);
			//pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{			
			MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
			resultString = serverObj.getPurchasedItemDetailByUserId(userId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			//pd.cancel();
			try{
				if(resultString != null){
					String itemIds = "";
					ArrayList<String> itemList = new ArrayList<String>();

					if(resultString != null){//start if
						//got app unlock status , changes on 5 May 2014
						String[] resultValue = resultString.split("&");
						resultString = resultValue[0].replace("&", "");
						String appUnlockStatus = resultValue[1].replace("&", "");

						if(resultString.length() > 0)
						{
							//itemIds = result.getItems().replace(",", "");
							//get item value which is comma separated , add (,) at the last for finish
							String newItemList = resultString + ",";

							for(int i = 0 ; i < newItemList.length() ; i ++ )
							{
								if(newItemList.charAt(i) == ',' )
								{
									itemList.add(itemIds);
									itemIds = "";
								}
								else
								{
									itemIds = itemIds +  newItemList.charAt(i);
								}
							}
						}

						ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();


						for( int i = 0 ; i < itemList.size() ; i ++ )
						{
							PurchaseItemObj purchseObj = new PurchaseItemObj();
							purchseObj.setUserId(userId);
							purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
							purchseObj.setStatus(1);
							purchaseItem.add(purchseObj);
						}

						//add app unlick statuc , changes on 5 May 2014
						try{
							if(!appUnlockStatus.equals("-1")){
								PurchaseItemObj purchseObj = new PurchaseItemObj();
								purchseObj.setUserId(userId);
								purchseObj.setItemId(100);
								purchseObj.setStatus(Integer.parseInt(appUnlockStatus));
								purchaseItem.add(purchseObj);
							}
						}catch(Exception e){
							Log.e(TAG, "Error while inserting app unlock status " + e.toString());
						}

						LearningCenterimpl learningCenter = new LearningCenterimpl(context);
						learningCenter.openConn();
						learningCenter.deleteFromPurchaseItem();
						learningCenter.insertIntoPurchaseItem(purchaseItem);
						learningCenter.closeConn();
					}//end if
				}
			}catch(Exception e){
				Log.e(TAG, "Error while getting purchase response " + e.toString());
			}
			super.onPostExecute(result);
		}
	}


	/**
	 * use to check player for challenge
	 */
	@SuppressWarnings("unused")
	private void checkPlayer()
	{
		/*Intent intent = new Intent(this,FriendzyNotificationPlayerActivity.class);
		startActivity(intent);*/
		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{
			UserPlayerOperation userPlayer = new UserPlayerOperation(this);
			if(!userPlayer.isUserPlayersExist())
			{
				SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
				if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
				{		
					/*Intent intent = new Intent(this,LoginUserPlayerActivity.class);
				startActivity(intent);*/

					UserRegistrationOperation userObj = new UserRegistrationOperation(this);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(this,TeacherPlayer.class);
						startActivity(intent);
					}
					else
					{
						Intent intent = new Intent(this,LoginUserPlayerActivity.class);
						startActivity(intent);
					}
				}
				else
				{
					if(AppVersion.CURRENT_VERSION == AppVersion.PAID_VERSION){
						SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(IS_CHECKED_PREFF, 0);
						String userId = sharedPreffPlayerInfo.getString("userId", "");
						if(getApplicationUnLockStatus(MAX_ITEM_ID_FOR_ALL_APP, userId) == 1)
						{
							/*UserRegistrationOperation userObj = new UserRegistrationOperation(this);
							if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
							{
								Intent intent = new Intent(this,TeacherChallengeActivity.class);
								startActivity(intent);
							}
							else
							{*/		 
							Intent intent = new Intent(this,StudentChallengeActivity.class);
							startActivity(intent);										
							//}
						}
					}else{
						if(CommonUtils.isInternetConnectionAvailable(this)){
							new GetRequiredCoinsForPurchaseItem().execute(null,null,null);
						}else{
							CommonUtils.showInternetDialog(this);
						}		
					}
				}
			}else{
				Intent intent = new Intent(this,LoginUserCreatePlayer.class);
				startActivity(intent);
			}
		}else{
			DialogGenerator dg = new DialogGenerator(this);
			Translation translate = new Translation(this);
			translate.openConnection();
			dg.generateWarningDialog(translate.getTranselationTextByTextIdentifier
					("alertMsgYouMustRegisterLoginToAccessThisFeature")+"");
			translate.closeConnection();
		}

	}


	/**
	 * This method check for application is unlock or not
	 * @param itemId
	 * @param userId
	 * @return
	 */
	private int getApplicationUnLockStatus(int itemId,String userId)
	{	
		int appStatus;
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}

	/**
	 * This class get coins from server
	 * @author Yashwant Singh
	 *
	 */
	class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;
		String userId;
		String playerId;

		GetRequiredCoinsForPurchaseItem()
		{			
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(IS_CHECKED_PREFF, 0);
			userId = sharedPreffPlayerInfo.getString("userId", "");
			playerId = sharedPreffPlayerInfo.getString("playerId", "");

			apiString = "userId="+userId+"&playerId="+playerId;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(MainActivity.this);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getSubscriptionInfoForUser(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pg.cancel();	

			if(coindFromServer.getAppStatus() == 1)
			{
				ArrayList<PurchaseItemObj> purchaseItem	= new ArrayList<PurchaseItemObj>();
				PurchaseItemObj obj						= new PurchaseItemObj();
				obj.setItemId(100);
				obj.setStatus(1);
				obj.setUserId(userId);
				purchaseItem.add(obj);
				LearningCenterimpl learnObj = new LearningCenterimpl(MainActivity.this);
				learnObj.openConn();
				learnObj.insertIntoPurchaseItem(purchaseItem);
				UserRegistrationOperation userObj = new UserRegistrationOperation(MainActivity.this);
				/*if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
				{
					Intent intent = new Intent(MainActivity.this,TeacherChallengeActivity.class);
					startActivity(intent);
				}
				else
				{*/		 
				Intent intent = new Intent(MainActivity.this,StudentChallengeActivity.class);
				startActivity(intent);										
				//}
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(MainActivity.this);
				dg.generateDialogForFriendzyUnlock(coindFromServer.getCoinsEarned(),coindFromServer);	
			}					

			super.onPostExecute(result);
		}
	}

	/**
	 * Set the subscription remaining days
	 */
	@SuppressWarnings("deprecation")
	private void setSubscriptionRemainingDays(){
		try{
			if(MathFriendzyHelper.isUserLogin(this)){
				boolean isPurchase = false;
				String subscriptionDateString = MathFriendzyHelper.getPurchaseSubscriptionDate(this);
				String expireDateString = MathFriendzyHelper.getSubscriptionExpireDate(this);
				Date subscriptionDate = MathFriendzyHelper.getValidateDate
						(subscriptionDateString , CURRENT_SUBSCRIPTION_DATE_FORMAT);
				if(subscriptionDate != null) {
					subscriptionDate.setHours(0);
					subscriptionDate.setMinutes(0);
					subscriptionDate.setSeconds(0);
					Date expireDate = MathFriendzyHelper.getValidateDate
							(expireDateString, CURRENT_EXPIRE_DATE_FORMAT);
					if (!MathFriendzyHelper.isEmpty(subscriptionDateString)) {
						if (subscriptionDate.compareTo(expireDate) > 0) {
							isPurchase = true;
						}
					}
				}

				if(CommonUtils.LOG_ON)
					Log.e(TAG , "date subscriptionDateString" + subscriptionDateString
							+ " expireDateString " + expireDateString + " " + isPurchase);

				if(isPurchase){
					MathFriendzyHelper.initializeIsSuscriptionPurchase(true);
					int days = MathFriendzyHelper.getFreeSubscriptionRemainingDays(this ,
							subscriptionDateString , CURRENT_SUBSCRIPTION_DATE_FORMAT);
					if (MathFriendzyHelper.getUserAccountType(this) == MathFriendzyHelper.TEACHER) {
						if(days > 0){
							txtFreeSubscriptionText.setText(lblYourSchoolSubscriptionExpire + " "
									+ MathFriendzyHelper.formatDataInGivenFormat(subscriptionDateString,
											CURRENT_SUBSCRIPTION_DATE_FORMAT, REQUIRED_EXPIRE_DATE_FORMAT));
							this.setVisibilityOfSubscriptionTextLayout(true);
						}else{
							this.setVisibilityOfSubscriptionTextLayout(false);
						}
					}else{
						this.setVisibilityOfSubscriptionTextLayout(false);
					}
				}else {
					MathFriendzyHelper.initializeIsSuscriptionPurchase(false);
					int days = MathFriendzyHelper.getFreeSubscriptionRemainingDays(this);
					if (days > 0) {                       
						txtFreeSubscriptionText.setText(lblFreeStudentTeacherFunctionExpire + " "
								+ MathFriendzyHelper.formatDataInGivenFormat(expireDateString,
										CURRENT_EXPIRE_DATE_FORMAT, REQUIRED_EXPIRE_DATE_FORMAT));
						this.setVisibilityOfSubscriptionTextLayout(true);
					} else {
						this.setVisibilityOfSubscriptionTextLayout(false);
					}
				}
			}else{
				txtFreeSubscriptionText.setText(lblRegisterLoginToUseSchoolFree);
				this.setVisibilityOfSubscriptionTextLayout(false);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Set the subscription text layout visibility
	 * @param isShow
	 */
	private void setVisibilityOfSubscriptionTextLayout(boolean isShow){
		if(isShow){
			freeSubscriptiontextLayout.setVisibility(RelativeLayout.VISIBLE);
		}else{
			freeSubscriptiontextLayout.setVisibility(RelativeLayout.GONE);
		}
	}
	
	private void clickOnVedioAndLessons() {
		startActivity(new Intent(this , ActLessonCategories.class));		
	}
	
	private void clickOnRequestATutor() {		
		//startActivity(new Intent(this , ActRequestATutor.class));
		startActivity(new Intent(this , ActPlayVideo.class));
	}	
	
	private void getInAppPurchaseStatus() {
        if(CommonUtils.isInternetConnectionAvailable(this)
                && MathFriendzyHelper.isUserLogin(this)
                && MathFriendzyHelper.isNeedToGetPurchaseStatus(this)){
            MathFriendzyHelper.getVideoInAppStatusSaveIntoSharedPreff(this ,
                    MathFriendzyHelper.getUserId(this) , new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {

                        }
                    } , false);
        }
    }
		
	private void clickOnCheckout() {
		//MathFriendzyHelper.showWarningDialog(this, "Message");
		Intent intent = new Intent(this , ActHelpSchoolFriendzy.class);
		startActivity(intent);
	}
}
