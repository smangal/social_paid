package com.mathfriendzy.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;

import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.language.Language;
import com.mathfriendzy.model.language.translation.Translation;

/**
 * This AsyncTask execute at the starting of the project in which the translation , languages from the server loaded 
 * into the database
 * @author Yashwant Singh
 *
 */
public class StartUpAsyncTask extends AsyncTask<Void, Void, Void> 
{		
	//private static boolean isLoadDatabase 			= false;
	private static boolean isGetLanguageFromServer 	= false;
	private static boolean isTranselation 			= false;
	private ProgressDialog pd = null;

	//private final String TAG = this.getClass().getSimpleName();
	private Context context = null;

	public StartUpAsyncTask(Context context)
	{
		MainActivity.isAvtarLoaded = false;
		this.context = context;		
		pd = CommonUtils.getProgressDialog(context);
	}

	@Override
	protected void onPreExecute()
	{
		pd.setMessage("Please be patient. This could take several minutes.");
		pd.setCancelable(false);
		pd.show();
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) 
	{
		SharedPreferences sheredPreference = context.getSharedPreferences("SHARED_FLAG", 0);	
		isGetLanguageFromServer = sheredPreference.getBoolean("isGetLanguageFromServer", false);
		isTranselation			= sheredPreference.getBoolean("isTranselation", false);

		SharedPreferences.Editor editor = sheredPreference.edit();

		/*
		 * use to return because no need to go inside
		 */
		if(isGetLanguageFromServer && isTranselation)
		{
			return null;
		}

		if(CommonUtils.isInternetConnectionAvailable(context))
		{
			if(!isGetLanguageFromServer)
			{
				editor.putBoolean("isGetLanguageFromServer", true);			
				//Log.e(TAG, "isGetLanguageFromServer");
				Language languageFromServer = new Language(context);
				languageFromServer.getLanguagesFromServer();
				languageFromServer.saveLanguages();
	
			}

			if(!isTranselation)
			{
				editor.putBoolean("isTranselation", true);
				editor.commit();
				//Log.e(TAG, "isTranselation");
				Translation traneselation = new Translation(context);
				traneselation.getTransalateTextFromServer(CommonUtils.LANGUAGE_CODE, CommonUtils.LANGUAGE_ID, CommonUtils.APP_ID);
				traneselation.saveTranslationText();

			}			
		}
		
		//Log.e("", "inside doInBackGround()");

		/*if(!isLoadDatabase)
		{
			InputStream is = null;
			OutputStream os = null;
			URL url;

			if(this.createFolder(DATABASE_PATH_ON_DEVICE))
			{
				try 
				{
					url = new URL(DATABASE_PATH_ON_INTERNET);
					is = url.openStream();
					os = new FileOutputStream(DATABASE_PATH_ON_DEVICE	+ "/" + DATABASE_NAME);
					try 
					{
						HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();

						byte[] buffer = new byte[1024];
						int length;
						while ((length = is.read(buffer)) > 0) 
						{
							os.write(buffer, 0, length);

						}
						Log.e("", "database copied.");
						os.flush();
					} 
					catch (Exception e1) 
					{
						e1.printStackTrace();
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
				isLoadDatabase = true;	
			}
		}
		else
			Log.e("", "File Not created");*/


		return null;
	}

	@Override
	protected void onPostExecute(Void result) 
	{		
		//starting main Activity after completion of background task
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() 
			{
				pd.cancel();
				Intent intent = new Intent(context,MainActivity.class);
				context.startActivity(intent);
				((Activity)context).finish();
			}
		}, 1000);
		
		super.onPostExecute(result);
	}


	/**
	 * This method creates the file directory and also check for existence
	 * @param fileDirectory
	 * @return
	 */
	/*private boolean createFolder(String fileDirectory)
	{
		File file = new File(fileDirectory);
		boolean isDirectoryCreated = false;

		if(file.exists())
			return true;
		else
			isDirectoryCreated = file.mkdirs();

		return isDirectoryCreated;
	}*/
}
