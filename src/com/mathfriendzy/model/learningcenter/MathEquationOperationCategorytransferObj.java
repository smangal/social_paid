package com.mathfriendzy.model.learningcenter;

public class MathEquationOperationCategorytransferObj 
{
	int equationId;
	int operationId;
	int categoryId;
	int grade;
	
	public int getEquationId() {
		return equationId;
	}
	public void setEquationId(int equationId) {
		this.equationId = equationId;
	}
	public int getOperationId() {
		return operationId;
	}
	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
}
