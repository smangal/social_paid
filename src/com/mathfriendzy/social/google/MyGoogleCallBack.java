package com.mathfriendzy.social.google;

public interface MyGoogleCallBack {
	
	public void onProfileData(GoogleProfile profile);
}
